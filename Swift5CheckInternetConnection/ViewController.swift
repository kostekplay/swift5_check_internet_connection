////  ViewController.swift
//  Swift5CheckInternetConnection
//
//  Created on 18/10/2020.
//  
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if NetworkMonitor.shared.isConnected {
            print("You are connected.")
        } else {
            print("Connection error.")
        }
        
    }

}

